
#include <stdio.h>

#define CHECK(e) { int res = (e); if (res) printf("CUDA ERROR %d\n", res); }
#define MAX(a, b) (a > b ? a : b)
#define CHANNEL 3

struct Image
{
  int width;
  int height;
  unsigned int bytes;
  unsigned char *img;
  int* dev_width;
  int* dev_height;
  unsigned int* dev_bytes;
  unsigned char *dev_img;
};


// Reads a color PPM image file (name provided), and
// saves data in the provided Image structure.
// The max_col_val is set to the value read from the
// input file. This is used later for writing output image.
int readInpImg (const char* fname, Image& source, int& max_col_val)
{

  FILE *src;

  if (!(src = fopen(fname, "rb")))
  {
    printf("Couldn't open file %s for reading.\n", fname);
    return 1;
  }

  char p,s;
  fscanf(src, "%c%c\n", &p, &s);

  if (p != 'P' || s != '6')   // Is it a valid format?
  {
    printf("Not a valid PPM file (%c %c)\n", p, s);
    exit(1);
  }

  fscanf(src, "%d %d\n", &source.width, &source.height);
  fscanf(src, "%d\n", &max_col_val);

  int pixels = source.width * source.height;
  source.bytes = pixels * 3;  // 3 => colored image with r, g, and b channels
  source.img = (unsigned char *)malloc(source.bytes);

  if (fread(source.img, sizeof(unsigned char), source.bytes, src) != source.bytes)
  {
  	printf("Error reading file.\n");
  	exit(1);
  }

  fclose(src);
  return 0;
}

// Write a color image into a file (name provided) using PPM file format.
// Image structure represents the image in the memory.
int writeOutImg(const char* fname, const Image& roted, const int max_col_val)
{

  FILE *out;

  if (!(out = fopen(fname, "wb")))
  {
    printf("Couldn't open file for output.\n");
    return 1;
  }

  fprintf(out, "P6\n%d %d\n%d\n", roted.width, roted.height, max_col_val);

  if (fwrite(roted.img, sizeof(unsigned char), roted.bytes , out) != roted.bytes)
  {
    printf("Error writing file.\n");
    return 1;
  }

  fclose(out);
  return 0;
}

int plot(const char* fname, int blockSize, float timeTaken)
{
	FILE* plot;

	if(!(plot = fopen(fname, "a")))
	{
		printf("Couldn't open plot file.\n");
		return 1;
	}

	fprintf(plot, "%d %f\n", blockSize, timeTaken);
	fclose(plot);
	return 0;
}

__global__
void rotateImage(unsigned char* img, unsigned char* rotated, int* width, int* height, unsigned int* bytes)
{
  //Work out the pixel this thread is working on
  int pixelX = (blockIdx.x * blockDim.x) + threadIdx.x;
  int pixelY = (blockIdx.y * blockDim.y) + threadIdx.y;

  //Check thread is in bounds of image
  if(pixelX < *width && pixelY < *height)
  {
    //Calculate offset of this pixel and colour channel in array of bytes
    int widthBytes = *width * CHANNEL;
    int rowOffset = pixelY * widthBytes;
    int colOffset = pixelX * CHANNEL;
    int channelOffset = threadIdx.z;
    int totalOffset = rowOffset + colOffset + channelOffset;

    //Fetch the value of this pixel channel
    int value = img[totalOffset];

    //Calculate offset of corresponding pixel & colour channel in rotated image
    int rotX = (*height - 1) - pixelY;
    int rotY = pixelX;
    int rotWidthBytes = *height * CHANNEL;
    int rotRowOffset = rotY * rotWidthBytes;
    int rotColOffset = rotX * CHANNEL;
    int totalRotOffset = rotRowOffset + rotColOffset + channelOffset;

    //Set the pixel channel value of the rotated image
    rotated[totalRotOffset] = value;

  }
}

int main(int argc, char **argv)
{

  if (argc != 2)
  {
      printf("Usage: exec filename\n");
      exit(1);
  }

  char *fname = argv[1];

  //Read the input file
  Image source;
  int max_col_val;
  if (readInpImg(fname, source, max_col_val) != 0)  exit(1);

  //Device variable to store result
  unsigned char* dev_rotated;

  //Allocate image space on GPU
  CHECK(cudaMalloc((void**)&(source.dev_width), sizeof(int)));
  CHECK(cudaMalloc((void**)&(source.dev_height), sizeof(int)));
  CHECK(cudaMalloc((void**)&(source.dev_bytes), sizeof(unsigned int)));
  CHECK(cudaMalloc((void**)&(source.dev_img), source.bytes));
  CHECK(cudaMalloc((void**)&dev_rotated, source.bytes));

  //Copy image data to GPU
  CHECK(cudaMemcpy(source.dev_width, &(source.width), sizeof(int), cudaMemcpyHostToDevice));
  CHECK(cudaMemcpy(source.dev_height, &(source.height), sizeof(int), cudaMemcpyHostToDevice));
  CHECK(cudaMemcpy(source.dev_bytes, &(source.bytes), sizeof(unsigned int), cudaMemcpyHostToDevice));
  CHECK(cudaMemcpy(source.dev_img, source.img, source.bytes, cudaMemcpyHostToDevice));

  int keepRunning = 1;

  for(int i = 1; keepRunning; i++)
  {
	  //Compute grid & block dims
	  int blockSize = i;
	  int gridWidth = (int)ceil((float)source.width / blockSize);
	  int gridHeight = (int)ceil((float)source.height / blockSize);
	  
	  //int nBlocks = 160;
	  //int blockWidth = (int)ceil((double)source.width / nBlocks);
	  //int blockHeight = (int)ceil((double)source.height / nBlocks);

	  //printf("Grid size: %d x %d x 1, Block size: %d x %d x 3, Total # threads: %d\n", nBlocks, nBlocks, blockWidth, blockHeight, nBlocks * nBlocks * blockWidth * blockHeight * 3);
	  printf("Grid size: %d x %d x 1, Block size: %d x %d x 3, Total # threads: %d\n", gridWidth, gridHeight, blockSize, blockSize, gridWidth * gridHeight * blockSize * blockSize * 3);

	  float averageTime = 0;

	  for(int j = 0; j < 10; j++)
	  {
		  cudaEvent_t start, stop;
		  cudaEventCreate(&start);
		  cudaEventCreate(&stop);

		  //Get start time
		  cudaEventRecord(start, 0);

		  //Run kernel
		  rotateImage<<<dim3(gridWidth, gridHeight),dim3(blockSize, blockSize, 3)>>>(source.dev_img, dev_rotated, source.dev_width, source.dev_height, source.dev_bytes);

		  //Get stop time
		  cudaEventRecord(stop, 0);
		  cudaEventSynchronize(stop);

		  //Compute time taken
		  float timeTaken;
		  cudaEventElapsedTime(&timeTaken, start, stop);
		  printf("Time taken: %f\n", timeTaken);

		  averageTime += timeTaken / 10;
	  }

	  printf("Average time: %f\n", averageTime);
	  plot("plots\\plot.out", blockSize, averageTime);

	  keepRunning = i < MAX(source.width, source.height) && i*i < 1024;	  
  }

  

  //Copy rotated image to host
  //CHECK(cudaMemcpy(source.img, dev_rotated, source.bytes, cudaMemcpyDeviceToHost));

  //Swap image dimensions
  //int temp = source.width;
  //source.width = source.height;
  //source.height = temp;

  // Write rotated image to the output file
  if (writeOutImg("rotated.ppm", source, max_col_val) != 0) // For demonstration, the input file is written to a new file named "rotated.ppm"
   exit(1);

  //Free allocated memory (host and device)
  free(source.img);
  CHECK(cudaFree(source.dev_width));
  CHECK(cudaFree(source.dev_height));
  CHECK(cudaFree(source.dev_bytes));
  CHECK(cudaFree(source.dev_img));
  CHECK(cudaFree(dev_rotated));

  exit(0);
}
