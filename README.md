Setup
=====

1. Ensure you have the lastest version of [cuda](https://developer.nvidia.com/cuda-toolkit) installed on your machine.
2. Navigate to the cloned repository.
3. Compile the application

```sh
nvcc image_rotator.cu -o image_rotator
```

Running
=======

Repository icon made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
